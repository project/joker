<div id="<?php print $id; ?>" class="block block-<?php print $block->module ?>">
<?php print $wrappers_open; ?>

<?php if ($block->subject): ?>
  <?php print theme('block_title', $block->subject); ?>
<?php endif;?>

  <div class="content"><?php print $block->content ?></div>
  
<?php print $wrappers_close; ?>
</div>