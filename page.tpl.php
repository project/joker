<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language; ?>" xml:lang="<?php print $language->language; ?>">

<head>
<title><?php print $head_title; ?></title>
<?php print $head; ?>
<!--[if IE]><link rel="stylesheet" href="/<?php print $directory; ?>/blueprint/lib/ie.css" type="text/css" media="screen, projection"><![endif]-->
<?php print $styles; ?>
<?php print $scripts; ?>
<?php print $style; ?>
</head>

<body<?php print $body_class; ?>>
  <?php print $wrappers['body']['open']; ?>
  
  <div id="page">
    <?php print $wrappers['page']['open']; ?>
    <div id="page-margin"><!-- /page-margin -->

      <?php print $wrappers['header']['open']; ?>
      <div id="page-header">
        <div id="logo">
          <?php print $logo; ?>
        </div>
        <?php print $site_slogan; ?>
        <div id="navigation">
          <?php print $primary_links; ?>
          <?php print $secondary_links; ?>
        </div>
        <?php print $search_box; ?>
        <?php print $breadcrumb; ?>
        <?php print $header; ?>
      </div><!-- /page-header -->
      <?php print $wrappers['header']['close']; ?>
  
      <?php print $wrappers['page-body']['open']; ?>
      <div id="page-body">
        <div id="page-body-left">
  
          <div id="content-column">
          <?php print $wrappers['content-column']['open']; ?>
            <div class="inner">
              <?php print $title; ?>
              <?php if ($content_top): ?>
                <?php print $wrappers['content-column-top']['open']; ?>
                <div id="content-top">
                  <?php print $content_top; ?>
                </div>
                <?php print $wrappers['content-column-top']['close']; ?>
              <?php endif; ?>
              <?php if ($mission): ?><div id="mission"><?php print $mission; ?></div><?php endif; ?>
              <?php print $messages; ?>
              <?php if ($tabs): ?><div class="tabs float-container"><?php print $tabs; ?></div><?php endif; ?>
              <?php print $help; ?>
              <?php print $content; ?>
              <?php print $feed_icons; ?>
              <?php if ($content_bottom): ?>
                <?php print $wrappers['content-column-bottom']['open']; ?>
                <div id="contentBottom">
                  <?php print $content_bottom; ?>
                </div>
                <?php print $wrappers['content-column-bottom']['close']; ?>
              <?php endif; ?>
            </div>
          <?php print $wrappers['content-column']['close']; ?>
          </div><!-- /content-column -->
  
          <?php if ($left): ?>
            <div id="left-column">
              <?php print $wrappers['left-sidebar']['open']; ?>
              <div class="inner">
                <?php print $left; ?>
              </div>
              <?php print $wrappers['left-sidebar']['close']; ?>
            </div><!-- /left-column -->
            <?php endif; ?>
        </div><!-- /page-sub-body -->

        <?php if ($right): ?>
          <div id="right-column">
            <?php print $wrappers['right-sidebar']['open']; ?>
            <div class="inner">
              <?php print $right; ?>
            </div>
            <?php print $wrappers['right-sidebar']['close']; ?>
          </div><!-- /right-column -->
        <?php endif; ?>

      </div><!-- /page-body -->
      <?php print $wrappers['page-body']['close']; ?>

      <?php if ($footer || $footer_message): ?>
        <?php print $wrappers['footer']['open']; ?>
        <div id="page-footer">
          <div class="message">
            <?php print $footer_message; ?>
          </div>
          <?php print $footer; ?>
        </div>
        <?php print $wrappers['footer']['close']; ?>
      <?php endif; ?>

    </div><!-- /page-margin -->
    <?php print $wrappers['page']['close']; ?>
  </div><!-- /page -->

  <?php print $wrappers['body']['close']; ?>

  <?php print $closure; ?>
</body>
</html>