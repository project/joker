<?php



/**
 * If the theme key is not defined yet, we're on the theme settings page
 * and need to switch to whatever theme is being set up
 */
global $theme_key;
if (!$theme_key){
  $theme_key = arg(4);
  init_theme();
}


/**
 * Sets up the presets folder
 */
if (!file_exists(file_directory_path().'/'.$theme_key)){

  $target = file_directory_path().'/'.$theme_key;
  $source = path_to_theme().'/presets';
  
  mkdir($target);

  $source_dir = opendir($source);
  while($subdir_name = readdir($source_dir)){
    if (is_dir($source.'/'.$subdir_name) && !ereg('^\.{1,2}$', $subdir_name)){
      mkdir($target.'/'.$subdir_name);
      $target_folder = $target.'/'.$subdir_name;
      $settings_file = $source.'/'.$subdir_name.'/settings';
      $image_file = $source.'/'.$subdir_name.'/theme_image.png';
      file_copy($settings_file, $target_folder);
      file_copy($image_file, $target_folder);
    }
  }
}

/**
 * Sets up default theme settings if required
 */
if (is_null(theme_get_setting('settings'))){
  
  include_once('theme-settings.php');
  
  // Save default theme settings.
  variable_set(
    str_replace('/', '_', 'theme_'.$theme_key.'_settings'),
    _defaults()
  );
  
  // Force refresh of Drupal internals.
  theme_get_setting('', TRUE);
}