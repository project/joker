<?php
include_once('bootstrap.inc');



/**
 * Gets the Curacao settings form
 *
 * @param $settings
 *   a keyed array of theme settings
 * @return
 *   a keyed array representing the settings form
 */
function joker_settings($settings){

  global $theme_key;

  drupal_add_css(path_to_theme().'/style/theme-settings.css');

  // Load / Save preset
  if ($settings['preset'] == 'save' && $settings['preset-name']){
    $settings['preset'] = _save_preset($settings['preset-name']);
  } else if ($settings['preset'] != 'new' && !ereg($settings['preset-name'].'$', $settings['preset'])) {
    _load_preset($settings['preset']);
//    drupal_goto('/admin/build/themes/settings/joker');
  }

  // Reload settings if we've set up default values 
  if (is_null($settings['settings'])){
    $settings = theme_get_settings($theme_key);
    variable_set('theme-image-index', variable_get('theme-image-index', 0) + 1);
  }

  // Clear cache
  variable_del('element-wrappers');
  variable_del('theme-css-cache');

  // Upload image and get dimensions
  _upload_theme_image($settings);

  // Build settings form
  $form = _hidden_fields($settings);
  $form += _display_options($settings);
  $form += _themeing_options($settings);
  $form += _presets($settings);
  $form += _editor($settings);
  $form += _layout($settings);
  $form += _color_fieldset($settings);
  
  // Remember defaults
  if (!$settings['theme-image']){
    $form += _hidden_defaults($settings);
  }

  return $form;
}



// --------------------------------------------------------------- Form sections



function _hidden_fields($settings){
  
  return 
    array(
      'settings' => array(
        '#type' => 'hidden',
        '#value' => 'TRUE',
      ),
      
      'theme-image' => array(
        '#type' => 'hidden',
        '#value' => $settings['upload-theme-image']
          ? FALSE 
          : $settings['theme-image'],
      ),

      'elements' => array(
        '#prefix' => '<div class="hidden-field">',
        '#type' => 'textfield',
        '#maxlength' => 8000,
        '#default_value' => $settings['elements'],
        '#suffix' => '</div>',
      ),

      'image-width' => array(
        '#value' => $settings['image-width'],
        '#type' => 'hidden',
      ),
      'image-height' => array(
        '#value' => $settings['image-height'],
        '#type' => 'hidden',
      ),
    );
}



function _display_options($settings){
  return 
    array(
      'show-breadcrumb' => array(
        '#default_value' => $settings['show-breadcrumb'],
        '#suffix' => '<hr/>',
        '#title' => t('Show breadcrumb in header'),
        '#type' => 'checkbox',
      ),
    ); 
}



function _themeing_options($settings){
  return 
    array(
      'path-class' => array(
        '#default_value' => $settings['path-class'],
        '#description' => t(
          'Adds the first argument of the path as a class to the body tag to
           enable page specific themeing.'
        ),
        '#title' => t('Path based body class'),
        '#type' => 'checkbox',
      ),

      'alias-class' => array(
        '#default_value' => $settings['alias-class'],
        '#description' => t(
          'Adds the first argument of the path alias as a class to the body 
           tag to enable page specific themeing.'
        ),
        '#title' => t('Alias based body class'),
        '#type' => 'checkbox',
      ),
    );
}



function _presets($settings){
  return array(
    'presets' => array(
      '#type' => 'fieldset',
      '#title' => 'Presets',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#attributes' => array('id' => 'presets'),

      'preset' => array(
        '#default_value' => $settings['preset'],
        '#options' => _preset_options(),
        '#type' => 'select',
      ),
      'preset-name' => array(
        '#default_value' => '',
        '#type' => 'textfield',
      ),
      array('#value' => '<div class="buttons">'),
      'save' => array(
        '#value' => 'Save',
        '#type' => 'submit',
        '#submit' => array('joker_save_preset'),
      ),
      'load' => array(
        '#value' => 'Load',
        '#type' => 'submit',
        '#submit' => array('joker_load_preset'),
      ),
      'delete' => array(
        '#value' => 'Delete',
        '#type' => 'submit',
        '#submit' => array('joker_delete_preset'),
      ),
      array('#value' => '</div>'),
    ),
  );
}



function _hidden_defaults($settings){
  return array(
    'page-left-margin' => array(
      '#type' => 'hidden',
      '#value' => $settings['page-left-margin'],
    ),
    'page-right-margin' => array(
      '#type' => 'hidden',
      '#value' => $settings['page-right-margin'],
    ),
    'left-sidebar-width' => array(
      '#type' => 'hidden',
      '#value' => $settings['left-sidebar-width'],
    ),
    'right-sidebar-width' => array(
      '#type' => 'hidden',
      '#value' => $settings['right-sidebar-width'],
    ),
    'page-top-margin' => array(
      '#type' => 'hidden',
      '#value' => $settings['page-top-margin'],
    ),
    'page-bottom-margin' => array(
      '#type' => 'hidden',
      '#value' => $settings['page-bottom-margin'],
    ),
    'header-height' => array(
      '#type' => 'hidden',
      '#value' => $settings['header-height'],
    ),
    'footer-height' => array(
      '#type' => 'hidden',
      '#value' => $settings['footer-height'],
    ),
  );
}



function _editor($settings){
  
  $form = array(
    'editor' => array(
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => !isset($settings['theme-image']),
      '#title' => 'Imagery',
      'theme-image-upload' => array(
        '#title' => 'Source image',
        '#type' => 'file',
      ),
    ),
  );

  drupal_add_js(path_to_theme().'/script/farbtastic.js');
  drupal_add_js(path_to_theme().'/script/color.js');
  drupal_add_css(path_to_theme().'/style/farbtastic.css');
  
  if ($settings['theme-image']){

    drupal_add_css(path_to_theme().'/style/move.css');
    drupal_add_css(path_to_theme().'/style/resize.css');
    drupal_add_css(path_to_theme().'/style/editor.css');
    drupal_add_css(path_to_theme().'/style/slider.css');
    
    drupal_add_js(path_to_theme().'/script/drag.js');
    drupal_add_js(path_to_theme().'/script/move.js');
    drupal_add_js(path_to_theme().'/script/resize.js');
    drupal_add_js(path_to_theme().'/script/marker.js');
    drupal_add_js(path_to_theme().'/script/slider.js');
    drupal_add_js(path_to_theme().'/script/theme-settings.js');
    
    $form['editor']['gui'] = array(

      array('#value' => '<div id="theme-workspace">'),

      'tools' => array(
        '#prefix' => '<div class="tools">',
        '#suffix' => '</div>',
        _image_fieldset(),
      ),

      array('#value' => 
        '<div class="canvas">
           <div class="scroll">
             <img id="theme-image" src="/'.
               file_directory_path().'/'.$settings['theme-image'].
               '?'.variable_get('theme-image-index', 0).
             '">
             <div class="grid"></div>
             <div class="layer">
               <div id="top-margin" 
                 class="margin moveable move-y resizeable resize-tb contain-y"></div>
               <div id="bottom-margin" 
                 class="margin moveable move-y resizeable resize-tb contain-y"></div>
               <div id="left-margin" 
                 class="margin moveable move-x resizeable resize-lr contain-x"></div>
               <div id="right-margin" 
                 class="margin moveable move-x resizeable resize-lr contain-x"></div>
             </div>
           </div>
         </div>'
      ),
      
      array('#value' => '</div>')
    );
  }   
  
  return $form;
}



function _layout($settings){
  return array(
    'layout' => array(
      '#type' => 'fieldset',
      '#title' => 'Layout',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#attributes' => array('id' => 'layout-fieldset'),
    
      array('#value' => '<div id="layout-toolbar">'),

      'page-top-margin' => array(
        '#default_value' => $settings['page-top-margin'],
        '#type' => 'textfield',
        '#prefix' => '<div class="left"><label for="edit-page-top-margin">'.
          t('top margin').'</label>',
        '#suffix' => '</div>',
      ),
      'header-height' => array(
        '#default_value' => $settings['header-height'],
        '#type' => 'textfield',
        '#prefix' => '<div class="right">',
        '#suffix' => '<label for="edit-header-height">'.
          t('header height').'</label></div>',
      ),
  
      array('#value' => '<br />'),
  
      array('#value' => '<div class="center"><div class="left">'),
      'page-left-margin' => array(
        '#default_value' => $settings['page-left-margin'],
        '#prefix' => '<label for="edit-page-left-margin">'.
          t('page left margin').'</label>',
        '#type' => 'textfield',
        '#suffix' => '<br />',
      ), 
      'left-sidebar-width' => array(
        '#default_value' => $settings['left-sidebar-width'],
        '#type' => 'textfield',
        '#prefix' => '<label for="edit-left-sidebar-width">'.
          t('left sidebar width').'</label>',
      ), 
      array('#value' => '</div>'),
  
      array('#value' => '<div class="page-placeholder"></div>'),
  
      array('#value' => '<div class="right">'),
      'page-right-margin' => array(
        '#default_value' => $settings['page-right-margin'],
        '#type' => 'textfield',
        '#suffix' => '<label for="edit-page-right-margin">'.
          t('page right margin').'</label>',
      ),
      'right-sidebar-width' => array(
        '#default_value' => $settings['right-sidebar-width'],
        '#type' => 'textfield',
        '#suffix' => '<label for="edit-right-sidebar-width">'.
          t('right sidebar width').'</label>',
      ),
      array('#value' => '</div></div>'),
  
      array('#value' => '<br />'),
      
      'page-bottom-margin' => array(
        '#default_value' => $settings['page-bottom-margin'],
        '#type' => 'textfield',
        '#prefix' => '<div class="left"><label for="edit-page-bottom-margin">'.
          t('page bottom margin').'</label>',
        '#suffix' => '</div>',
      ),
      'footer-height' => array(
        '#default_value' => $settings['footer-height'],
        '#type' => 'textfield',
        '#prefix' => '<div class="right">',
        '#suffix' => '<label for="edit-footer-height">'.
          t('footer height').'</label></div>',
      ),
      
      'page-width' => array(
        '#prefix' => '<div class="both">',
        '#suffix' => '</div>',
        '#title' => 'Page width', 
        '#type' => 'textfield',
        '#default_value' => $settings['page-width'],
      ),
      
      array('#value' => '</div>'),
    ),
  );
}



function _grid_toolbar($settings){
  return array(
    array('#value' => '<div id="grid-toolbar">'),
      'show-grid' => array(
        '#default_value' => $settings['show-grid'],
        '#title' => 'Grid',
        '#type' => 'checkbox',
      ),
      'snap-to-grid' => array(
        '#default_value' => $settings['snap-to-grid'],
        '#title' => 'Snap',
        '#type' => 'checkbox',
      ),
      'size-x' => array(
        '#default_value' => $settings['size-x'] ? $settings['size-x'] : 10,
        '#size' => 2,
        '#title' => 'Size',
        '#type' => 'textfield',
        '#attributes' => array('class' => 'slider', 'max' => 10, 'min' => 1, 'dir' => 'horizontal'),
      ),
      'size-y' => array(
        '#default_value' => $settings['size-y'] ? $settings['size-y'] : 10,
        '#size' => 2,
        '#type' => 'textfield',
        '#attributes' => array('class' => 'slider', 'max' => 10, 'min' => 1, 'dir' => 'horizontal'),
      ),
      'offset-x' => array(
        '#default_value' => $settings['offset-x'] ? $settings['offset-x'] : 0,
        '#size' => 2,
        '#title' => 'Offset',
        '#type' => 'textfield',
        '#attributes' => array('class' => 'slider', 'max' => 10, 'min' => 0, 'dir' => 'horizontal'),
      ),
      'offset-y' => array(
        '#default_value' => $settings['offset-y'] ? $settings['offset-y'] : 0,
        '#size' => 2,
        '#type' => 'textfield',
        '#attributes' => array('class' => 'slider', 'max' => 10, 'min' => 0, 'dir' => 'horizontal'),
      ),
      array('#value' => '</div>'),
  );
}



function _image_fieldset(){
  return 
    array(
      'marker-fieldset' => array(
        '#attributes' => array('id' => 'marker-fieldset'),
        '#type' => 'fieldset',

        'element' => array(
          '#title' => 'Part of',
          '#type' => 'select',
          '#options' => _element_options(),
        ),
      
        'block' => array(
          '#attributes' => array('class' => 'disabled', 'disabled' => 'disabled'),
          '#type' => 'select',
          '#options' => _block_options(),
        ),

        array('#value' => '<br />',),

        'position' => array(
          '#options' => _position_options(),
          '#title' => 'Align',
          '#type' => 'select',
        ),
        'custom-position' => array(
          '#attributes' => array('class' => 'disabled', 'disabled' => 'disabled'),
          '#type' => 'textfield',
        ),
        
        'repeat' => array(
          '#options' => _repeat_options(),
          '#prefix' => '<div class="auto-label">',
          '#suffix' => '</div>',
          '#title' => 'Tile',
          '#type' => 'select',
        ),

        'z-index' => array(
          '#title' => 'Weight',
          '#type' => 'select',
          '#options' => _z_index_optons(),
        ),

        array('#value' => '<br />',),
        array('#value' => '<div class="auto-label">',),
          'width' => array(
            '#type' => 'textfield',
            '#title' => 'Width',
            '#default_value' => '100',
          ),
          'height' => array(
            '#type' => 'textfield',
            '#title' => 'Height',
            '#default_value' => '100',
          ),
  
          'x' => array(
            '#type' => 'textfield',
            '#title' => 'X',
            '#default_value' => '0',
          ),
          'y' => array(
            '#type' => 'textfield',
            '#title' => 'Y',
            '#default_value' => '0',
          ),
        array('#value' => '</div>',),
          
        array('#value' => '<br />',),

        array('#value' => '<div id="buttons">'),
        array(
          '#prefix' => '<div class="button-add-marker">',
          '#suffix' => '</div>',
          '#value' => '<input 
            id="button-add-marker"
            class="button"
            type="button"
            value="Add" />',
        ),
        array(
          '#prefix' => '<div class="button-remove-marker">',
          '#suffix' => '</div>',
          '#value' => '<input
            id="button-remove-marker"
            class="button disabled"
            type="button"
            value="Remove"
            disabled="disabled"/>'
        ),
        array('#value' => '</div>'),

      ),
  );
}



function _color_fieldset($settings){
  return array(
    'color' => array(
      '#type' => 'fieldset',
      '#title' => 'Color',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#attributes' => array('id' => 'color-fieldset'),
  
      'use-body-color' => array(
        '#prefix' => '<div class="form-item">',
        '#type' => 'checkbox',
        '#default_value' => $settings['use-body-color'],
      ),
      'body-color' => array(
        '#type' => 'textfield',
        '#title' => 'Body background',
        '#default_value' => $settings['body-color'],
        '#attributes' => array('class' => 'color-field'),
        '#suffix' => '</div><br />',
      ),

      'use-page-color' => array(
        '#prefix' => '<div class="form-item">',
        '#type' => 'checkbox',
        '#default_value' => $settings['use-page-color'],
      ),
      'page-color' => array(
        '#type' => 'textfield',
        '#title' => 'Page background',
        '#default_value' => $settings['page-color'],
        '#attributes' => array('class' => 'color-field'),
        '#suffix' => '</div>',
      ),

      'use-page-header-color' => array(
        '#prefix' => '<div class="form-item">',
        '#type' => 'checkbox',
        '#default_value' => $settings['use-page-header-color'],
      ),
      'page-header-color' => array(
        '#type' => 'textfield',
        '#title' => 'Page header background',
        '#default_value' => $settings['page-header-color'],
        '#attributes' => array('class' => 'color-field'),
        '#suffix' => '</div>',
      ),

      'use-page-body-color' => array(
        '#prefix' => '<div class="form-item">',
        '#type' => 'checkbox',
        '#default_value' => $settings['use-page-body-color'],
      ),
      'page-body-color' => array(
        '#type' => 'textfield',
        '#title' => 'Page body background',
        '#default_value' => $settings['page-body-color'],
        '#attributes' => array('class' => 'color-field'),
        '#suffix' => '</div>',
      ),

      'use-page-footer-color' => array(
        '#prefix' => '<div class="form-item">',
        '#type' => 'checkbox',
        '#default_value' => $settings['use-page-footer-color'],
      ),
      'page-footer-color' => array(
        '#type' => 'textfield',
        '#title' => 'Page footer background',
        '#default_value' => $settings['page-footer-color'],
        '#attributes' => array('class' => 'color-field'),
        '#suffix' => '</div><hr />',
      ),

      'primary-link-color' => array(
        '#type' => 'textfield',
        '#title' => 'Primary link color',
        '#default_value' => $settings['page-body-color'],
        '#attributes' => array('class' => 'color-field'),
      ),
      'secondary-link-color' => array(
        '#type' => 'textfield',
        '#title' => 'Secondary link color',
        '#default_value' => $settings['page-body-color'],
        '#attributes' => array('class' => 'color-field'),
        '#suffix' => '<hr />',
      ),
      'primary-tab-link-color' => array(
        '#type' => 'textfield',
        '#title' => 'Primary tab link color',
        '#default_value' => $settings['primary-tab-link-color'],
        '#attributes' => array('class' => 'color-field'),
      ),
      'secondary-tab-link-color' => array(
        '#type' => 'textfield',
        '#title' => 'Secondary tab link color',
        '#default_value' => $settings['secondary-tab-link-color'],
        '#attributes' => array('class' => 'color-field'),
        '#suffix' => '<hr />',
      ),
      'text-color' => array(
        '#type' => 'textfield',
        '#title' => 'Text color',
        '#default_value' => $settings['text-color'],
        '#attributes' => array('class' => 'color-field'),
      ),
      'link-color' => array(
        '#type' => 'textfield',
        '#title' => 'Link color',
        '#default_value' => $settings['link-color'],
        '#attributes' => array('class' => 'color-field'),
      ),
      'heading-color' => array(
        '#type' => 'textfield',
        '#title' => 'Heading color',
        '#default_value' => $settings['heading-color'],
        '#attributes' => array('class' => 'color-field'),
      ),
    ),
  );
}



// ------------------------------------------------------------- Submit handlers



function joker_save_preset($form_id, &$form){

  $parts = split('/', $form['values']['preset']);
  $preset = array_pop($parts);
  if ($preset == 'new'){
    $preset = ereg_replace('[^a-zA-Z0-9]', '-', $form['values']['preset-name']);
  }
  
  if ($preset) {
    global $theme_key;
    $output_dir = file_directory_path().'/'.$theme_key;
    $image_file = $output_dir.'/theme_image.png';
    
    if (file_exists($output_dir.'/'.$preset) || mkdir($output_dir.'/'.$preset)){
  
      file_copy($image_file, $output_dir.'/'.$preset.'/theme_image.png', FILE_EXISTS_REPLACE);
  
      $settings = theme_get_settings($theme_key);
      $settings['preset'] = $form['values']['preset'];
      file_put_contents($output_dir.'/'.$preset.'/settings', serialize(_clean_settings($settings)));
      _commit_settings($settings);

      drupal_set_message('The preset has been saved.');

    } else {
      drupal_set_message(
        'Could not create the preset folder '.
        $output_dir.'/'.
        $preset.'!'
      );
    }
  } else {
    drupal_set_message(
      'To save a preset you should supply a preset name!'
    );
  }
}



function joker_load_preset($form_id, &$form){
  
  $preset = $form['values']['preset'];

  global $theme_key;
  $output_dir = file_directory_path().'/'.$theme_key;
  $preset_image = $preset.'/theme_image.png';
  $image_file = $output_dir.'/theme_image.png';

  variable_set('theme-image-index', variable_get('theme-image-index', 0) + 1);
  
  file_copy($preset_image, $image_file, FILE_EXISTS_REPLACE);

  $file_contents = file_get_contents($preset.'/settings');
  $settings = unserialize($file_contents);

  _commit_settings($settings);

}



function joker_delete_preset($form_id, &$form){

  global $theme_key;
  $preset = $form['values']['preset'];
  
  $dir = opendir($preset);
  while($file = readdir($dir)){
    if ($file != '.' && $file != '..'){
      file_delete($preset.'/'.$file);
    }
  }
  
  rmdir($preset);

  $settings = theme_get_settings($theme_key);
  $settings['preset'] = 'new';
  
  _commit_settings($settings);

}



// ------------------------------------------------------------------- Constants



function _defaults(){
  return array(
    'settings' => TRUE,
    'page-width' => 950,
    'page-left-margin' => 0,
    'page-right-margin' => 0,
    'left-sidebar-width' => 180,
    'right-sidebar-width' => 180,
    'page-top-margin' => 0,
    'page-bottom-margin' => 0,
    'header-height' => 'auto',
    'footer-height' => 'auto',
  
    'body-color' => '#ffffff',
    'use-body-color' => 0,
    'page-color' => '#ffffff',
    'use-page-color' => 0,
    'page-header-color' => '#ffffff',
    'use-page-header-color' => 0,
    'page-body-color' => '#ffffff',
    'use-page-body-color' => 0,
    'page-footer-color' => '#ffffff',
    'use-page-footer-color' => 0,
  
    'primary-link-color' => '#222222',
    'secondary-link-color' => '#222222',
    'primary-tab-link-color' => '#222222',
    'secondary-tab-link-color' => '#222222',

    'text-color' => '#222222',
    'link-color' => '#000099',
    'heading-color' => '#000099',
  );
}



// --------------------------------------------------------------------- Options



function _block_options(){

  $options = array();
  $modules = module_implements('block');

  foreach($modules as $module){
    $blocks = module_invoke($module, 'block', 'list');
    if (count($blocks)){
      foreach($blocks as $delta=>$block){
        $options[strtolower($module.'-'.ereg_replace('[^a-zA-Z0-9]', '', $delta))] = $block['info'];
      }
    }
  }

  return $options;
}



function _preset_options(){

  global $theme_key;
  $output_dir = file_directory_path().'/'.$theme_key;
  $options = array(
    'new' => 'New preset',
  );

  if (!file_exists($output_dir)){
    mkdir($output_dir);
  }
  
  $dir = opendir($output_dir);
  while($file = readdir($dir)){
    if (is_dir($output_dir.'/'.$file) && !ereg('\.\.?', $file)){
      $options[$output_dir.'/'.$file] = $file;
    }
  }
  closedir($dir);

  return $options;
}



function _element_options(){
  return array(
    'logo' => 'Logo',
    'body' => 'HTML body',
    'page' => 'Page', 
    'header' => 'Page header', 
    'primary-link' => 'Primary link',
    'primary-link-active' => 'Active primary link',
    'header-blocks' => 'Header blocks', 
    'page-body' => 'Page body', 
    'left-sidebar' => 'Left sidebar', 
    'left-blocks' => 'Left sidebar block', 
    'content' => 'Content column',
    'content-top' => 'Content top', 
    'content-top-blocks' => 'Content top blocks', 
    'content' => 'Content area', 
    'primary-tabs' => 'Primary tabs',
    'secondary-tabs' => 'Secondary tabs',
    'content-bottom' => 'Content bottom', 
    'content-bottom-blocks' => 'Content bottom blocks', 
    'right-sidebar' => 'Right sidebar', 
    'right-blocks' => 'Right sidebar blocks', 
    'footer' => 'Page footer',
    'footer-blocks' => 'Footer blocks',
    'specific-block' => 'Specific block...',
  );
} 


function _position_options(){
  return array(
    'left top' => 'Top left',
    'center top' => 'Top center',
    'right top' => 'Top right',
    'left center' => 'Left center',
    'center center' => 'Center',
    'right center' => 'Right center',
    'left bottom' => 'Bottom left',
    'center bottom' => 'Bottom center',
    'right bottom' => 'Bottom right',
    'custom' => 'Custom...',
  );
}


function _repeat_options(){
  return array(
    'no-repeat' => 'no tile',
    'repeat-x' => 'horizontal',
    'repeat-y' => 'vertical',
    'repeat' => 'both',
  );  
}



function _z_index_optons(){
  return array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15);
}



// ----------------------------------------------------------------------- Utils



function _upload_theme_image(&$settings){
  if ($file = 
        file_save_upload(
          'theme-image-upload',
          array('file_validate_is_image' => array())
        )
      ){

    global $theme_key;
    $parts = pathinfo($file->filename);
    $filename = $theme_key.'/theme_image.'.$parts['extension'];
    
    $directory = realpath(file_directory_path()).'/'.$theme_key;
    if (!file_exists($directory) && !mkdir($directory)){
      drupal_set_message('Could not create directory '.$directory);
      return;
    }

    if (file_copy($file, $filename, FILE_EXISTS_REPLACE)) {
      
      drupal_set_message('Theme image imported.');

      drupal_set_message(realpath(file_directory_path().'/'.$filename));
      
      // Get theme image dimensions
      $image_path = realpath(file_directory_path().'/'.$filename);
      $image_dimension = getimagesize($image_path);

      $settings['theme-image'] = $filename;
      $settings['page-width'] = $image_dimension[0];
      $settings['image-width'] = $image_dimension[0];
      $settings['image-height'] = $image_dimension[1];
      $settings['theme-image-upload'] = FALSE;
      _commit_settings($settings);
      theme_get_setting('', TRUE);
      
      variable_set('theme-image-index', variable_get('theme-image-index', 0) + 1);
    }
  }
}



function _clean_settings($settings){
  // Don't save the toggle_node_info_ variables.
  if (module_exists('node')) {
    foreach (node_get_types() as $type => $name) {
      unset($settings['toggle_node_info_' . $type]);
    }
  }
  return $settings;
}



function _commit_settings($new_settings){

  global $theme_key;
  $settings = theme_get_settings($theme_key);
  $settings = _clean_settings($settings);
  $new_settings = _clean_settings($new_settings);

  // Save theme settings.
  variable_set(
    str_replace('/', '_', 'theme_'. $theme_key .'_settings'),
    array_merge($settings, $new_settings)
  );

  // Force refresh of Drupal internals.
  theme_get_setting('', TRUE);

}