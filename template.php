<?php
include_once('bootstrap.inc');



// --------------------------------------------------------------- Preprocessors



/**
 * Hooked to provide wrappers
 * @param $variables
 *   A keyed array of variables to pass to the page template
 *
 * @return array of variables
 */
function joker_preprocess($variables = array()) {

  $variables['wrappers'] = _wrappers();

  return $variables;
}



/**
 * Hooked to provide dynamic theme variables
 * @param $variables
 *   A keyed array of variables to pass to the page template
 *
 * @return array of variables
 */
function joker_preprocess_page($variables = array()) {

  _page_variables($variables);
  
  $variables['style'] = _css($variables); 

  return $variables;
}



/**
 * Hooked to add node-type-teaser.tpl.php to suggested templates
 * @param $variables
 *   A keyed array of variables to pass to the node template
 *
 * @return a keyed array of variables
 */
function joker_preprocess_node($variables = array()) {
  if($variables['teaser']) {
      $variables['template_files'] = 
        array('node-teaser', 'node-' . $variables['node']->type . '-teaser');
  }
  return $variables;
}



/**
 * Hooked to add block id to variables
 * @param $variables
 *   A keyed array of variables to pass to the node template
 *
 * @return a keyed array of variables
 */
function joker_preprocess_block($variables = array()) {

  $wrappers = _wrappers();
  $variables['id'] = _title_to_id($variables['block']->subject);
  $block_id = strtolower($variables['block']->module.'-'.ereg_replace('[^a-zA-Z0-9]', '', $variables['block']->delta));
  
  if ($wrappers[$block_id]){
    $variables['wrappers_open'] = $wrappers[$block_id]['open'];
    $variables['wrappers_close'] = $wrappers[$block_id]['close'];
  } else {
    $variables['wrappers_open'] = 
      $wrappers[$variables['block']->region.'-blocks']['open'];
    $variables['wrappers_close'] = 
      $wrappers[$variables['block']->region.'-blocks']['close'];
  }

  return $variables;
}



// ----------------------------------------------------------------- Theme hooks



/**
 * Return a themed form element.
 *
 * @param element
 *   An associative array containing the properties of the element.
 *   Properties used: title, description, id, required
 * @param $value
 *   The form element's data.
 * @return
 *   A string representing the rendered form element.
 */
function joker_form_element($element, $value) {
  
  $wrapper_class = !empty($element['#wrapper-class'])
    ? ' '.$element['#wrapper-class']
    : '';
  
  $output  = '<div class="form-item'.$wrapper_class.'">'."\n";
  $required = !empty($element['#required']) 
    ? '<span class="form-required" title="'.
        t('This field is required.').
      '">*</span>' 
    : '';

  if (!empty($element['#title'])) {
    $title = $element['#title'];
    if (!empty($element['#id'])) {
      $output .= 
        ' <label for="'. $element['#id'] .'">'.
          t('!title: !required', array(  
              '!title' => filter_xss_admin($title), 
              '!required' => $required
          )) ."</label>\n";
    }
    else {
      $output .= ' <label>'. t('!title: !required', array(
        '!title' => filter_xss_admin($title), 
        '!required' => $required)
      ) ."</label>\n";
    }
  }

  $output .= " $value\n";

  if (!empty($element['#description'])) {
    $output .= '<div class="description">'.$element['#description']."</div>\n";
  }

  $output .= "</div>\n";

  return $output;
}



/**
 * Implementation of theme_table
 * Wraps tables to provide horisontal scrolling if needed
 */
function joker_table(
    $header, $rows, $attributes = array(), $caption = NULL){
  return
    '<div class="table-wrapper">'.
    theme_table($header, $rows, $attributes, $caption).
    '</div>';
}



// ---------------------------------------------------------- Graphical themeing



function joker_logo(){
  
  $wrappers = _wrappers();
  
  $url = $wrappers['#images']['logo-wrapper-0']
    ? $wrappers['#images']['logo-wrapper-0']['image']
    : theme_get_setting('logo');
 
  $title = t('Home');
  return module_exists('page_elements')
    ? theme('graphical_logo', $url, $title)
    : '<a href="/" title="'.$title.'">'.
        '<img src="'.$url.'" alt="'.$title.'"/>'.
      '</a>';
}

function joker_primary_links(){

  $wrappers = _wrappers();
  $primary_links = menu_primary_links();

  $rendered = '<ul id="primary-links">';
  foreach ($primary_links as $primary_link) {
    
    $attributes = $primary_link['attributes']
      ? $primary_link['attributes']
      : array();
    
    $link = l(
      $primary_link['title'],
      $primary_link['href'],
      $attributes
    );
    
    $active = ereg('class=".*active.*"', $link)
      ? '-active'
      : '';
    
  	$rendered .= '<li>'.
      $wrappers['primary-link'.$active]['open'].
  	  $open_wrapper.
  	  $link.
      $wrappers['primary-link'.$active]['close'].
	  '</li>';
  }
  $rendered .= '</ul>';

  return $rendered;
}

function joker_secondary_links(){

  $wrappers = _wrappers();
  $secondary_links = menu_secondary_links();

  $rendered = '<ul id="secondary-links">';
  foreach ($secondary_links as $secondary_link) {
    
    $attributes = $secondary_link['attributes']
      ? $secondary_link['attributes']
      : array();
    
    $link = l(
      $secondary_link['title'],
      $secondary_link['href'],
      $attributes
    );
    
    $active = ereg('class=".*active.*"', $link)
      ? '-active'
      : '';
    
    $rendered .= '<li>'.
      $wrappers['secondary-link'.$active]['open'].
      $open_wrapper.
      $link.
      $wrappers['secondary-link'.$active]['close'].
    '</li>';
  }
  $rendered .= '</ul>';

  return $rendered;
}

function joker_page_title($title){
  return module_exists('page_elements')
    ? theme('graphical_page_title', $title)
    : '<h1 class="title">'.$title.'</h1>';
}

function joker_block_title($title){
  return module_exists('page_elements')
    ? theme('graphical_block_title', $title)
    : '<h2>'.$title.'</h2>';
}

function joker_menu_local_tasks(){
  return module_exists('page_elements')
    ? theme('graphical_menu_local_tasks')
    : theme_menu_local_tasks();
}

function joker_button($element){
  return module_exists('page_elements')
    ? theme('graphical_button', $element)
    : theme_button($element);
}

function joker_menu_item_link($link) {
  return module_exists('page_elements')
    ? theme('graphical_menu_item_link', $link)
    : theme_menu_item_link($link);
}

function joker_theme(){
  return array(
    'logo' => array(),
    'primary_links' => array(),
    'secondary_links' => array(),
    'page_title' => array('title' => FALSE),
    'block_title' => array('title' => FALSE),
    'button' => array('element' => array()),
  );
}



// ----------------------------------------------------------- Utility functions



function _layout_css(){

  $variables = array(
    'page_width' => theme_get_setting('page-width'),
    'top_margin' => 0 + theme_get_setting('page-top-margin'),
    'right_margin' => 0 + theme_get_setting('page-right-margin'),
    'bottom_margin' => theme_get_setting('page-bottom-margin'),
    'left_margin' => 0 + theme_get_setting('page-left-margin'),
    'horizontal_margin' => 0 + 
      theme_get_setting('page-left-margin') + 
      theme_get_setting('page-right-margin'),
    'header_height' => theme_get_setting('header-height') == 'auto' 
      ? 'auto'
      : (0 + theme_get_setting('header-height')).'px',
    'left_column_width' => theme_get_setting('left-sidebar-width'),
    'right_column_width' => theme_get_setting('right-sidebar-width'),
    'footer_height' => theme_get_setting('footer-height') == 'auto' 
      ? 'auto' 
      : (0 + theme_get_setting('footer-height')).'px',
  );

  extract($variables, EXTR_SKIP);
  ob_start();
  include('style/page-layout.css.php');
  $css = ob_get_contents();
  ob_end_clean();
  
  return $css;
}



function _background_css(){

  $wrappers = _wrappers();

  if ($wrappers['#images']){
    $wrappers = _wrappers();
    $variables = array(
      'images' => $wrappers['#images'],
      'theme_image_index' => variable_get('theme-image-index', 0),
    );
    extract($variables, EXTR_SKIP);
    ob_start();
    include('style/images.css.php');
    $css = ob_get_contents();
    ob_end_clean();
  }

  return $css;
}



function _color_css(){

  $variables = array(
    'body_color' => theme_get_setting('body-color'),
    'use_body_color' => theme_get_setting('use-body-color'),
    'page_color' => theme_get_setting('page-color'),
    'use_page_color' => theme_get_setting('use-page-color'),
    'page_header_color' => theme_get_setting('page-header-color'),
    'use_page_header_color' => theme_get_setting('use-page-header-color'),
    'page_body_color' => theme_get_setting('page-body-color'),
    'use_page_body_color' => theme_get_setting('use-page-body-color'),
    'page_footer_color' => theme_get_setting('page-footer-color'),
    'use_page_footer_color' => theme_get_setting('use-page-footer-color'),

    'primary_link_color' => theme_get_setting('primary-link-color'),
    'secondary_link_color' => theme_get_setting('secondary-link-color'),
    'primary_tab_link_color' => theme_get_setting('primary-tab-link-color'),
    'secondary_tab_link_color' => theme_get_setting('secondary-tab-link-color'),
    'text_color' => theme_get_setting('text-color'),
    'link_color' => theme_get_setting('link-color'),
    'heading_color' => theme_get_setting('heading-color'),
  );

  extract($variables, EXTR_SKIP);
  ob_start();
  include('style/color.css.php');
  $css = ob_get_contents();
  ob_end_clean();
  
  return $css;
}



function _css(){
  
  $css = variable_get('theme-css-cache', FALSE);
  
  if (!$css) {
    
    $css = 
      '<style type="text/css">'.
        _layout_css().
        _background_css().
        _color_css().
      '</style>';

    variable_set('theme-css-cache', $css);
  }
  
  return $css;
}



/**
 * Adds wrappers for the page elements
 *
 * @return $wrappers
 *   Keyed array of wrapper information for each wrap region
 */
function _wrappers(){

  static $failed = FALSE;
  if (!theme_get_setting('elements', FALSE) || $failed){
    return;
  }

  static $wrappers = FALSE;
  if (!$wrappers){
    $wrappers = variable_get('element-wrappers', FALSE);
  }

  // Build if needed
  if (!$wrappers){

    global $theme_key;
    $wrappers = array('#images' => '');
    $wrappers_setup = explode('|', theme_get_setting('elements', FALSE));
    $image_extention = 
      substr(theme_get_setting('theme-image', 'png'), strlen($image_path) - 3);
    $image_path = realpath(
      file_directory_path().'/'.$theme_key.'/theme_image.'.$image_extention);
    $image = FALSE;

    switch ($image_extention){
      case 'gif': $image = imagecreatefromgif($image_path); break;
      case 'jpg': $image = imagecreatefromjpeg($image_path); break;
      case 'png': $image = imagecreatefrompng($image_path); break;
      default:
        drupal_set_message(
          $theme_key.' can only read png, jpg and gif files! :' + $image_path,
          'error'
        );
        $failed = TRUE;
        return;
    }
    
    if (!$image) {
      $failed = TRUE;
      drupal_set_message(
        $theme_key.' could not open '.$image_path.' for reading!', 
        'error'
      );
      return;
    }

    // Clean out the image directory
    $output_dir =
        file_directory_path().
        '/'.$theme_key;
    $dir = opendir($output_dir);
    while($file = readdir($dir)){
      if (substr($file, strlen($file) - 3) == 'png' && 
          $file != 'theme_image.png'){
        file_delete($output_dir.'/'.$file);
      }
    }
    
    foreach($wrappers_setup as $wrapper_setup){
      // $wrapper_setup = element:block,position:custom,repeat,z-index,width,height,x,y
      $attributes = explode(',', $wrapper_setup);

      // For specific blocks
      $element_parts = split(':', $attributes[0]);
      $position_parts = split(':', $attributes[1]);
      $wrapper_name = '';
      if ($element_parts[0] == 'specific-block'){
        $wrapper_name = _title_to_id($element_parts[1]);
      } else {
        $wrapper_name = $element_parts[0];
      }
      $wrapper_id = $wrapper_name.'-wrapper-'.(0 + $wrappers[$wrapper_name]['count']);
      
      // Cut out wrapper image
      $cache_id = md5(
        $attributes[4].
        $attributes[5].
        $attributes[6].
        $attributes[7]
      );
      $output_file =
        '/'.$wrapper_id.'-'.
        $cache_id.'.png';
        
      $fragment = imagecreatetruecolor(
        (int)$attributes[4], 
        (int)$attributes[5]
      );
      imagecopy(
        $fragment, 
        $image, 
        0, 0, 
        (int)$attributes[6], (int)$attributes[7], 
        (int)$attributes[4], (int)$attributes[5]
      );
      
      imagepng($fragment, realpath($output_dir).$output_file);
      imagedestroy($fragment);
      
      // Generate wrapper HTML
      $wrappers[$wrapper_name] = $wrappers[$wrapper_name]
        ? $wrappers[$wrapper_name]
        : array(
            'count' => 0,
          );
      $wrappers[$wrapper_name]['open'][$attributes[3]] .=
        '<div '.
          'class="'.
            $wrapper_name.'-wrapper-'.
            $wrappers[$wrapper_name]['count'].'"'.
        '>';
      $wrappers[$wrapper_name]['close'] .= '</div>';

      $wrappers['#images'][$wrapper_id] = array(
        'id' => $wrapper_id,
        'image' => '/'.$output_dir.$output_file,
        'repeat' => $attributes[2],
        'position' => $position_parts[0] == 'custom' 
          ? $position_parts[1]
          : $position_parts[0],
        'width' => $attributes[4],
        'height' => $attributes[5],
      );

      $wrappers[$wrapper_name]['count']++;
    }

    // Collapse opening wrappers to string
    foreach($wrappers as $element=>$wrapper){
      if ($element != '#images'){
        ksort($wrappers[$element]['open']);
        $wrappers[$element]['open'] = implode($wrappers[$element]['open']);
      } 
    }

    imagedestroy($image);

    variable_set('element-wrappers', $wrappers);
  }

  return $wrappers;
}



/**
 * Adds page custom page variables
 *
 * @param $variables
 *   keyed array of existing page variables
 */
function _page_variables(&$variables){
  
  // Toggles
  $variables['logo'] = theme_get_setting('toggle_logo')
    ? theme('logo')
    : FALSE;
  $variables['primary_links'] = theme_get_setting('toggle_primary_links')
    ? theme('primary_links')
    : FALSE;
  $variables['secondary_links'] = theme_get_setting('toggle_secondary_links')
    ? theme('secondary_links')
    : FALSE;
  $variables['breadcrumb'] = theme_get_setting('show-breadcrumb')
    ? $variables['breadcrumb']
    : FALSE;
  $variables['title'] = $variables['title']
    ? theme('page_title', $variables['title'])
    : FALSE;
    
  if (theme_get_setting('toggle_slogan')) {
    $variables['site_slogan'] = variable_get('site_slogan', '');
  }
  
  // Body classes
  $alias = array_shift(explode('/', drupal_get_path_alias($_GET['q'])));
  $body_class['path'] = theme_get_setting('path-class')
    ? 'path-'.arg(0) : '';
  $body_class['alias'] = theme_get_setting('alias-class') && $alias
    ? 'alias-'.arg(0) : '';

  $variables['body_class'] = 
    ' class="'.
    $variables['body_classes'].
    (count($body_class) > 0
      ? ' '
      : ''
    ).
    implode(' ', $body_class).'"';
}



/**
 * Converts a title into an id
 *
 * @param string $title
 */
function _title_to_id($title){
  return ereg_replace('[\ ]+', '-', 
            ereg_replace('[^a-z0-9\ \-]*', '', 
              strtolower(
                trim(
                  htmlspecialchars_decode($title, ENT_QUOTES)
                )
              )
            )
          );
}


