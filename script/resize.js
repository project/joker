
// Script to provide elements with sizing capability
// TODO: implemented OOP



var resizeStartHeight;
var resizeStartWidth;
var resizeStartX;
var resizeStartY;


$(
  function(){
    $('div.resizeable').each(
      function(){
        addDragResizeBehaviour($(this));
      }
    );
  }
);



function addHandles(element){

  var handles = new Array(
    'top-left', 
    'top', 
    'top-right', 
    'left', 
    'right', 
    'bottom-left', 
    'bottom', 
    'bottom-right'
  );
  var handleClasses = new Array('tl', 't', 'tr', 'l', 'r', 'bl', 'b', 'br');
  var handleClass = element.attr('class').match(/resize-[trbl]+/);
  var showHandles = handleClass ? ('' + handleClass).split(/-/)[1] : '';

  for (var index in handles){
    if (!handleClass || showHandles.match(handleClasses[index]))
    element.prepend('<div class="handle ' + handles[index] + '" />');
  }
}



function addDragResizeBehaviour(element){
  addHandles(element);
  element.children('.handle').each(
    function(){
      $(this).mousedown(
        function(event){
          var handle = $(this);
          resizeStartHeight = cleanNumber(handle.parent().css('height'));
          resizeStartWidth = cleanNumber(handle.parent().css('width'));
          resizeStartX = cleanNumber(handle.parent().css('left'));
          resizeStartY = cleanNumber(handle.parent().css('top'));
          drag(handle, event);
          return false;
        }
      );
      $(this).bind('drag', resizeElement);
    }
  );
}



function resizeElement(event, dragEvent){

  var handle = $(this);
  var element = handle.parent();
  
  var height = cleanNumber(element.css('height'));
  var left = cleanNumber(element.css('left'));
  var top = cleanNumber(element.css('top'));
  var width = cleanNumber(element.css('width'));
  
  switch(true){

    case handle.hasClass('top-left'):
      left = resizeStartX + dragEvent.offsetX;
      top = resizeStartY + dragEvent.offsetY;
      height = resizeStartHeight - dragEvent.offsetY;
      width = resizeStartWidth - dragEvent.offsetX;
    break;

    case handle.hasClass('top'):
      height = resizeStartHeight - dragEvent.offsetY;
      top = resizeStartY + dragEvent.offsetY;
    break;

    case handle.hasClass('top-right'):
      height = resizeStartHeight - dragEvent.offsetY;
      top = resizeStartY + dragEvent.offsetY;
      width = resizeStartWidth + dragEvent.offsetX;
    break;

    case handle.hasClass('left'):
      left = resizeStartX + dragEvent.offsetX;
      width = resizeStartWidth - dragEvent.offsetX;
    break;

    case handle.hasClass('right'):
      width = resizeStartWidth + dragEvent.offsetX;
    break;

    case handle.hasClass('bottom-left'):
      height = resizeStartHeight + dragEvent.offsetY;
      left = resizeStartX + dragEvent.offsetX;
      width = resizeStartWidth - dragEvent.offsetX;
    break;

    case handle.hasClass('bottom'):
      height = resizeStartHeight + dragEvent.offsetY;
    break;

    case handle.hasClass('bottom-right'):
      height = resizeStartHeight + dragEvent.offsetY;
      width = resizeStartWidth + dragEvent.offsetX;
    break;
  }

  // Make sure the element does not resize negatively 
  if (handle.attr('class').match(/left/) && width < 0) {
    width = 0;
    left = resizeStartX + resizeStartWidth;
  }
  if (handle.attr('class').match(/top/) && height < 0) {
    height = 0;
    top = resizeStartY + resizeStartHeight;
  }
  if (handle.attr('class').match(/right/) && width < 0) {
    width = 0;
    left = resizeStartX;
  }
  if (handle.attr('class').match(/bottom/) && height < 0) {
    height = 0;
    top = resizeStartY;
  }

  // Contain the element to its parent on the Y axis
  if (element.attr('class').match(/contain-x?y/)){
    var containerHeight = cleanNumber(element.parent().css('height'));
    var borderHeight = 
      cleanNumber(element.css('border-top-width')) +
      cleanNumber(element.css('border-bottom-width'));
    
    if (top < 0) { height = height + top; top = 0;}
    if (top + height + borderHeight > containerHeight) { 
      height = containerHeight - top - borderHeight;
    }
  }

  // Contain the element to its parent on the X axis
  if (element.attr('class').match(/contain-xy?/)){
    var containerWidth = cleanNumber(element.parent().css('width'));
    var borderWidth =
      cleanNumber(element.css('border-left-width')) +
      cleanNumber(element.css('border-right-width'));
    
    if (left < 0) { 
      width = width + left; left = 0;
    }
    if (left + width + borderWidth > containerWidth) { 
      width = containerWidth - left - borderWidth;
    }
  }

  element.css('top', top);
  element.css('left', left);
  element.css('height', height);
  element.css('width', width);
  
  element.trigger('resize');
}


