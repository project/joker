
// Script to provide elements with moving capability
// TODO: implement OOP



var moveStartX;
var moveStartY;



$(
  function(){
    $('div.moveable').each(
      function(){
        addDragMoveBehaviour($(this));
      }
    );
  }
);



function addDragMoveBehaviour(element){

  element.mousedown(
    function(event){
      var element = $(this); 
      moveStartX = cleanNumber(element.css('left'));
      moveStartY = cleanNumber(element.css('top'));
      drag(element, event);
      return false;
    }
  );
  element.bind('drag', moveElement);
}



function moveElement(event, dragEvent){

  var element = $(this);
  
  if (!dragEvent){
    return;
  }
  
  var newX = moveStartX + dragEvent.offsetX;
  var newY = moveStartY + dragEvent.offsetY;
  
  if (element.attr('class').match(/contain-xy?/)){
    var width =
      cleanNumber(element.css('width')) +
      cleanNumber(element.css('border-left-width')) +
      cleanNumber(element.css('border-right-width'));
    var parentWidth = cleanNumber(element.parent().css('width'));
    if (newX + width > parentWidth){
      newX = parentWidth - width;
    } 
    if (newX < 0){
      newX = 0;
    } 
  }

  if (element.attr('class').match(/contain-x?y/)){
    var height = 
      cleanNumber(element.css('height')) + 
      cleanNumber(element.css('border-top-width')) +
      cleanNumber(element.css('border-bottom-width'));
    var parentHeight = cleanNumber(element.parent().css('height'));
    if (newY + height > parentHeight){
      newY = parentHeight - height;
    } 
    if (newY < 0){
      newY = 0;
    } 
  }

  if (element.attr('class').match(/move-xy?/) || 
      (
        !element.attr('class').match(/move-[xy]{1,2}/) && 
        element.hasClass('moveable')
      )
     ){
    element.css('left', newX + 'px');
  }
  
  if (element.attr('class').match(/move-x?y/) || 
      (
        !element.attr('class').match(/move-[xy]{1,2}/) && 
        element.hasClass('moveable')
      )
     ){
    element.css('top', newY + 'px');
  }
  
  element.trigger('move');
}


