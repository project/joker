
$(
  function(){
    initDisplay();
    initFormBehaviour();
    loadMarkers();
  }
);



// ------------------------------------------------------------------ Behaviours



function initFormBehaviour(){

  // Update layout
  $('#theme-workspace .margin, #theme-workspace .margin *').bind(
    'drag', updateLayoutForm
  );
  $('#layout-toolbar input.form-text').keyup(updateLayout);

  // Update grid
  $('#edit-show-grid').click(function(){toggleGrid()});
  $('#edit-snap-to-grid').click(function(e){updateGrid(e, $(this))});
  $('#grid-toolbar input').change(function(e){updateGrid(e, $(this))});

  // Add marker
  $('#button-add-marker').click(addMarker);

  // Update marker on form change
  $('#marker-fieldset input').keyup(updateMarker);
  $('#marker-fieldset select').change(updateMarker);

  // Update buttons on element selection
  $('#marker-fieldset #edit-element').change(updateButtons);

  // Deselect marker
  $('#theme-workspace .layer, #theme-workspace .margin').mousedown(function(event){unselectCurrentMarker(event);});

  // Remove marker
  $('#button-remove-marker').click(removeCurrentMarker);

  // Save settings
  $('form').submit(submitForm);

  // Enable/disable element fields
  $('#edit-element, #edit-position').change(toggleFields);
  toggleFields();
}



function toggleFields(){

  var specificBlock = $('#edit-block');
  if ($('#edit-element').val() == 'specific-block'){
    specificBlock.
      removeAttr('disabled').
      removeClass('disabled').
      focus();
  } else {
    specificBlock.
      attr('disabled', true).
      addClass('disabled');
  }
  
  var customPosition = $('#edit-custom-position');
  if ($('#edit-position').val() == 'custom'){
    customPosition.
      removeAttr('disabled').
      removeClass('disabled').
      focus();
  } else {
    customPosition.
      attr('disabled', 'disabled').
      addClass('disabled', true);
  }
}



function submitForm(){
  var elements = $('#edit-elements');
  elements.val('');
  $('#theme-workspace .canvas .layer .marker').each(
    function(){
      var marker = $(this);
      elements.val(
        (elements.val() + marker.children('input').val() + '|')
      );
    }
  );
  elements.val(elements.val().replace(/\|$/, ''));
}



// --------------------------------------------------------------------- Markers



function loadMarkers(){

  var elements_val = $('#edit-elements').val();
  var elements = elements_val.split('|');

  if (elements_val.length){
    for(var i = 0; i < elements.length; i++){
      data = elements[i].split(',');
      createMarker(
        data[0],
        data[1],
        data[2],
        data[3],
        data[4],
        data[5],
        data[6],
        data[7]
      );
    }
  }  
}



function addMarker(){
  createMarker(
    $('#edit-element').val() + ':' + $('#edit-block').val(),
    $('#edit-position').val() + ':' + $('#edit-custom-position').val(),
    $('#edit-repeat').val(),
    $('#edit-z-index').val(),
    $('#edit-width').val(),
    $('#edit-height').val(),
    $('#edit-x').val(),
    $('#edit-y').val()
  );

  updateButtons();
}



function updateMarker(){

  if ($('#edit-element').val() == 'logo'){
    var logoMarker = $('#logo-image');
    var logo_exists = logoMarker.length;
    var logo_is_current = currentMarker &&
      ($('#logo-image').get(0) == currentMarker.get(0));
    if (logo_exists && !logo_is_current){
      selectMarker(logoMarker);
    }
    return;
  }

  updateCurrentMarker(
    $('#edit-element').val() + ':' + $('#edit-block').val(),
    $('#edit-position').val() + ':' + $('#edit-custom-position').val(),
    $('#edit-repeat').val(),
    $('#edit-z-index').val(),
    $('#edit-width').val(),
    $('#edit-height').val(),
    $('#edit-x').val(),
    $('#edit-y').val()
  );
}



function updateMarkerForm(){
  if (currentMarker){
    data = currentMarker.children('input').val().split(',');
    element = data[0].split(':');
    position = data[1].split(':');
  
    $('#edit-element').val(element[0]);
    $('#edit-block').val(element[1]);
    $('#edit-position').val(position[0]);
    $('#edit-custom-position').val(position[1]);
    $('#edit-repeat').val(data[2]);
    $('#edit-z-index').val(data[3]);
  
    $('#edit-width').val(
      cleanNumber(currentMarker.css('width'))
      + cleanNumber(currentMarker.css('border-left-width'))
      + cleanNumber(currentMarker.css('border-right-width'))
    );
    $('#edit-height').val(
      cleanNumber(currentMarker.css('height'))
      + cleanNumber(currentMarker.css('border-top-width'))
      + cleanNumber(currentMarker.css('border-bottom-width'))
    );
  
    $('#edit-x').val(cleanNumber(currentMarker.css('left')));
    $('#edit-y').val(cleanNumber(currentMarker.css('top')));
  }
}



// --------------------------------------------------------------------- Display



function initDisplay(){

  // Set the grid and layer width & height
  var themeImageWidth = $('#edit-image-width').val();
  var themeImageHeight = $('#edit-image-height').val();

  var grid_layer = $('#theme-workspace div.grid, #theme-workspace div.layer'); 
  grid_layer.css('width', themeImageWidth + 'px');
  grid_layer.css('height', themeImageHeight + 'px');

  updateDisplay();
}



function updateDisplay(){
  updateLayout();
  toggleGrid();
  updateGrid();
}



function updateLayout(){

  var themeImageWidth = $('#edit-image-width').val();
  var themeImageHeight = $('#edit-image-height').val();

  $('#top-margin, #bottom-margin').css('width', themeImageWidth + 'px');
  $('#top-margin, #bottom-margin').css('left', '0px');

  var topMargin = $('#top-margin'); 
  topMargin.css('top', $('#edit-page-top-margin').val() + 'px');
  topMargin.css('height', $('#edit-header-height').val() == 'auto'
    ? 'auto'
    : $('#edit-header-height').val() + 'px'
  );

  var bottomMargin = $('#bottom-margin');  
  bottomMargin.css('top',
    themeImageHeight -
    $('#edit-page-bottom-margin').val() -
    cleanNumber($('#edit-footer-height').val()) - 2 + 'px'
  );
  bottomMargin.css('height', $('#edit-footer-height').val() == 'auto'
    ? 'auto'
    : $('#edit-footer-height').val() + 'px'
  );


  $('#right-margin, #left-margin').css('height', themeImageHeight + 'px');

  var leftMargin = $('#left-margin'); 
  leftMargin.css('left', 
  cleanNumber($('#edit-page-left-margin').val()) + 'px');
  leftMargin.css('width', 
    cleanNumber($('#edit-left-sidebar-width').val()) + 'px');
  
  var rightMargin = $('#right-margin'); 
  rightMargin.css('left', 
    themeImageWidth - 
    $('#edit-page-right-margin').val() -
    $('#edit-right-sidebar-width').val() - 2 + 'px'
  );
  rightMargin.css('width', $('#edit-right-sidebar-width').val() + 'px');
}



function updateLayoutForm(){

  var topMargin = $('#top-margin');
  var bottomMargin = $('#bottom-margin');
  var rightMargin = $('#right-margin');
  var leftMargin = $('#left-margin');

  if (cleanNumber(topMargin.css('height')) == '0'){
    $('#edit-header-height').val('auto');
  } else {
    $('#edit-header-height').val(cleanNumber(topMargin.css('height')));
  }
  if (cleanNumber(bottomMargin.css('height')) == '0'){
    $('#edit-footer-height').val('auto');
  } else {
    $('#edit-footer-height').val(cleanNumber(bottomMargin.css('height')));
  }

  $('#edit-left-sidebar-width').val(cleanNumber(leftMargin.css('width')));
  $('#edit-right-sidebar-width').val(cleanNumber(rightMargin.css('width')));

  $('#edit-page-top-margin').val(cleanNumber(topMargin.css('top')));
  $('#edit-page-left-margin').val(cleanNumber(leftMargin.css('left')));

  $('#edit-page-bottom-margin').val(
    $('#edit-image-height').val() - 
    cleanNumber(bottomMargin.css('top')) - 
    cleanNumber(bottomMargin.css('height')) -
    cleanNumber(bottomMargin.css('border-top-width')) - 
    cleanNumber(bottomMargin.css('border-bottom-width')) 
  );

  $('#edit-page-right-margin').val(
    $('#edit-image-width').val() - 
    cleanNumber(rightMargin.css('left')) - 
    cleanNumber(rightMargin.css('width')) -
    cleanNumber(rightMargin.css('border-left-width')) - 
    cleanNumber(rightMargin.css('border-right-width')) 
  );
}



function updateButtons(){
  var addButton = $('#button-add-marker');
  var removeButton = $('#button-remove-marker');
  
  if (currentMarker){
    addButton.val('Clone');
    enableButton(removeButton);

    if (currentMarker.attr('id') == 'logo-image'){
      disableButton(addButton);
    } else {
      enableButton(addButton);
    }

  } else {
    addButton.val('Add');
    if ($('#logo-image').length && $('#edit-element').val() == 'logo'){
      disableButton(addButton);
    } else {
      enableButton(addButton);
    }
    disableButton(removeButton);
  }
}



function disableButton(button){
  button.attr('disabled', true).addClass('disabled');
}



function enableButton(button){
  button.removeAttr('disabled').removeClass('disabled');
}



// ------------------------------------------------------------------------ Grid



function toggleGrid(){

  var control = $('#edit-show-grid');
  var grid = $('#theme-workspace div.grid');

  if (control.attr('checked')){
    grid.fadeIn('slow');
  } else {
    grid.fadeOut('slow');
  }
}



function updateGrid(e, control){

  var snap = $('#edit-snap-to-grid');
  var xOffset = $('#edit-offset-x');
  var yOffset = $('#edit-offset-y');
  var grid = $('#theme-workspace div.grid');

  if (snap.attr('checked')){
    dragSettings.gridSizeX = 10;
    dragSettings.gridSizeY = 10;
  } else {
    dragSettings.gridSizeX = 1;
    dragSettings.gridSizeY = 1;
  } 

  grid.css('background-position', 
    (xOffset.attr('value') % 10) + 'px ' + 
    (yOffset.attr('value') % 10) + 'px'
  );
}


