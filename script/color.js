
$(
  function initColorPickers(){
    $('.color-field').each(
      function(){
        var field = $(this);
        field.parent().append('<div class="picker"></div>');
        var picker = field.next();
        picker.farbtastic(field);
        field.focus(function(){picker.children().show();});
        field.blur(function(){picker.children().hide();});
      }
    );
  }
);