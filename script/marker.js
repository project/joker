
var blockOptions;
var elementOptions;
var positionOptions;

var currentMarker = false;

$(
  function(){
    blockOptions = $('#edit-block').attr('options');
    elementOptions = $('#edit-element').attr('options');
    positionOptions = $('#edit-position').attr('options');
  }
);



function createMarker(element, position, repeat, zIndex, width, height, x, y){

  var dataString =
    element + ',' + position + ',' + repeat + ',' +
    zIndex + ',' +
    width + ',' + height + ',' +
    x + ',' + y;

  $('#theme-workspace .layer').append(
    '<div class="marker moveable move-xy resizeable contain-xy">' +
      '<input type="hidden" value="' + dataString + '" />' +
      '<ul class="tooltip">' +
        '<li class="element">' + retrieveElementLabel(element) + '</li>' + 
        '<li class="position">' + (element.match(/^logo/) ? '' : retrievePositionLabel(position)) + '</li>' + 
      '</ul>' +
    '</div>'
  );

  marker = $('#theme-workspace .layer .marker:last');

  if (element.match(/^logo/)){
    marker.attr('id', 'logo-image');
  }
  marker.css('left', x + 'px');
  marker.css('top', y + 'px');
  marker.css('width', width
    - cleanNumber(marker.css('border-left-width'))
    - cleanNumber(marker.css('border-right-width'))
    + 'px');
  marker.css('height', height
    - cleanNumber(marker.css('border-top-width'))
    - cleanNumber(marker.css('border-bottom-width'))
    + 'px');

  marker.css('z-index', zIndex);
  
  addDragMoveBehaviour(marker);
  addDragResizeBehaviour(marker);

  marker.mousedown(function(){selectMarker($(this));});
  marker.mouseover(function(){hoverMarker($(this));});
  marker.mouseout(function(){dimMarker($(this));});
  
  marker.bind('drag', updateMarkerTransform);
  marker.bind('resize', updateMarkerTransform);
  
  selectMarker(marker);
  
  return marker;
}



function updateCurrentMarker(){

  var element = $('#edit-element').val() + ':' + $('#edit-block').val();
  var position = $('#edit-position').val() + ':' + $('#edit-custom-position').val();
  var repeat = $('#edit-repeat').val();
  var zIndex = $('#edit-z-index').val();
  var width = $('#edit-width').val();
  var height = $('#edit-height').val();
  var x = $('#edit-x').val();
  var y = $('#edit-y').val();

  if (currentMarker) {

    currentMarker.children('input').val(
      element + ',' + position + ',' + repeat + ',' +
      zIndex + ',' +
      width + ',' + height + ',' +
      x + ',' + y
    );

    if (element.match(/^logo/)){
      currentMarker.attr('id', 'logo-image');
    } else {
      currentMarker.removeAttr('id');
    }

    currentMarker.children('ul.tooltip').
      children('li.element').text(retrieveElementLabel(element));
    currentMarker.children('ul.tooltip').
      children('li.position').text(element.match(/^logo/)
        ? '' 
        : retrievePositionLabel(position)
      );

    currentMarker.css('left', x + 'px');
    currentMarker.css('top', y + 'px');
    currentMarker.css('width', width
      - cleanNumber(currentMarker.css('border-left-width'))
      - cleanNumber(currentMarker.css('border-right-width'))
      + 'px');
    currentMarker.css('height', height
      - cleanNumber(currentMarker.css('border-top-width'))
      - cleanNumber(currentMarker.css('border-bottom-width'))
      + 'px');
    currentMarker.css('z-index', zIndex);
  }
}



function updateMarkerTransform(){
  updateMarkerForm();
  updateCurrentMarker();
}


function removeCurrentMarker(){
  if (currentMarker){
    currentMarker.remove();
    unselectCurrentMarker(currentMarker);
  }
}



function selectMarker(marker){

  if (currentMarker && (currentMarker.get(0) != marker.get(0))){
    unselectCurrentMarker();
  }

  marker.css('border-color', 'red');
  marker.children('.handle').fadeIn('fast');
  marker.children('.tooltip').fadeIn('fast');
  marker.unbind('mouseover');
  marker.unbind('mouseout');
  currentMarker = marker;

  updateMarkerForm();
  updateButtons();
}



function unselectCurrentMarker(event){

  if (currentMarker){
    currentMarker.css('border-color', '#cdcdcd');
    currentMarker.children('.handle').fadeOut('slow');
    currentMarker.children('.tooltip').fadeOut('slow');
    currentMarker.mouseover(function(){hoverMarker($(this));});
    currentMarker.mouseout(function(){dimMarker($(this));});
  
    currentMarker = false;
  }
  
  if (event){
    updateButtons();
  }
}



function hoverMarker(marker){
  marker.children('.handle').fadeIn('fast');
  marker.children('.tooltip').fadeIn('fast');
}



function dimMarker(marker){
  marker.children('.handle').fadeOut('fast');
  marker.children('.tooltip').fadeOut('fast');
}



// -------------------------------------------------------------- HTML accessors



function retrieveElementLabel(element){

  var elementLabel = '';
  var value = element.split(':');

  if (value[0] == 'specific-block') {
    for (var i = 0; i < blockOptions; i++){
      if (blockOptions[i].value == value[1]){
        elementLabel = blockOptions[i].text;
      }
    }
  } else {
    for (var i = 0; i < elementOptions.length; i++){
      if (elementOptions[i].value == value[0]){
        elementLabel = elementOptions[i].text;
      }
    }
  }
  
  return elementLabel;
}



function retrievePositionLabel(position){

  if (position){
    var positionLabel = '';
    var value = position.split(':');
    
    if (value[0] == 'custom') {
      return value[1];
    } else {
      for (var i = 0; i < positionOptions.length; i++){
        if (positionOptions[i].value == value[0]){
          positionLabel = positionOptions[i].text;
        }
      }
    }
  } else {
    positionLabel = '';
  }
  return positionLabel;
}


