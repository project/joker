
// Script to handle mouse dragging of elements
// TODO: implement OOP



var dragElement;
var dragStartX;
var dragStartY;
var dragOffsetX;
var dragOffsetY;

var dragSettings = new Object;
dragSettings.gridSizeX = 1;
dragSettings.gridSizeY = 1;



function drag(element, event){

  dragStartX = event.pageX;
  dragStartY = event.pageY;

  $(document).bind('mousemove', dragHandler);
  $(document).mouseup(
    function(){
      $(document).unbind('mousemove', dragHandler);
    }
  );

  dragElement = element;
}



function dragHandler(event){

  dragOffsetX = parseInt(
    cleanNumber(event.pageX - dragStartX) / dragSettings.gridSizeX
  ) * dragSettings.gridSizeX;
  dragOffsetY = parseInt(
    cleanNumber(event.pageY - dragStartY) / dragSettings.gridSizeY
  ) * dragSettings.gridSizeY;

  var dragEvent = new Object;
  dragEvent.offsetX = cleanNumber(dragOffsetX);  
  dragEvent.offsetY = cleanNumber(dragOffsetY);
  
  dragElement.trigger('drag', dragEvent);
}



function cleanNumber(property){

  try{
    if (!property){
      return 0;
    }

    var cleaned = parseInt(('' + property).replace(/[^0-9.\-\+]/,'')); 
    if (isNaN(cleaned)){
      return 0;
    } else {
      return cleaned;
    }
  } catch(err){}

  return 0;
}