
$(
  function(){
    $('input.slider').each(
      function(){
        slide($(this));
      }
    );
  }
);



function slide(element){

  element.after('<div class="slider"><div class="moveable move-x contain-xy"></div></div>');

  var slider = element.next('div.slider');
  var sliderIndicator = slider.children('div');

  addDragMoveBehaviour(sliderIndicator);
  sliderIndicator.bind('move', updateValue);

  slider.css('width', element.css('width'));

  var maxValue = cleanNumber(element.attr('max'));
  var minValue = cleanNumber(element.attr('min'));
  var value = cleanNumber(element.val());
  var sliderWidth =
    cleanNumber(slider.css('width'))
    - cleanNumber(sliderIndicator.css('width'))
    - cleanNumber(sliderIndicator.css('border-left-width'))
    - cleanNumber(sliderIndicator.css('border-right-width'));
  var sliderPos = (value - minValue) / (maxValue - minValue) * sliderWidth;

  sliderIndicator.css('left', sliderPos + 'px');
  sliderIndicator.css('height',
    cleanNumber(slider.css('height')) -
    cleanNumber(sliderIndicator.css('border-top-width')) -
    cleanNumber(sliderIndicator.css('border-bottom-width')) + 'px'
  );
}


function updateValue(){

  var sliderIndicator = slider = $(this);
  var slider = sliderIndicator.parent();
  var element = slider.prev('input'); 
  var maxValue = cleanNumber(element.attr('max'));
  var minValue = cleanNumber(element.attr('min'));
  
  var sliderWidth =
    cleanNumber(slider.css('width'))
    - cleanNumber(sliderIndicator.css('width'))
    - cleanNumber(sliderIndicator.css('border-left-width'))
    - cleanNumber(sliderIndicator.css('border-right-width'));
  var sliderPos = 
    cleanNumber(sliderIndicator.css('left'));
    
  var value = parseInt((sliderPos / sliderWidth) * (maxValue - minValue) + minValue); 
  element.val(value);
  element.trigger('change');
}


