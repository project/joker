<?php if ($use_body_color) {?>body{ background-color: <?php print $body_color; ?>;}<?php } ?>

<?php if ($use_page_color) {?>body div#page{ background-color: <?php print $page_color; ?>;}<?php } ?>
<?php if ($use_page_header_color) {?>body div#page-header{ background-color: <?php print $page_header_color; ?>;}<?php } ?>
<?php if ($use_page_body_color) {?>body div#page-body{ background-color: <?php print $page_body_color; ?>;}<?php } ?>
<?php if ($use_page_footer_color) {?>body div#page-footer{ background-color: <?php print $page_footer_color; ?>;}<?php } ?>

body ul#primary-links a{ color: <?php print $primary_link_color; ?>;}
body ul#secondary-links a{ color: <?php print $secondary_link_color; ?>;}

body div.tabs ul.primary a{ color: <?php print $primary_tab_link_color; ?>;}
body div.tabs ul.secondary a{ color: <?php print $secondary_tab_link_color; ?>;}

body{ color: <?php print $text_color; ?>;}
a{ color: <?php print $link_color; ?>;}
h1, h2, h3, h, h5, h6 { color: <?php print $heading_color; ?>;}
