
@CHARSET "UTF-8";

body{ margin: 0; text-align: center;}
#page{ margin: 0 auto; min-height: 600px; text-align: left; width: <?php print $page_width; ?>px;}
#page-margin{ margin: 0 <?php print $right_margin; ?>px 0 <?php print $left_margin; ?>px; padding: <?php print $top_margin; ?>px 0 <?php print $bottom_margin; ?>px 0;}
#page-header{ height: <?php print $header_height; ?>; position: relative;}

#page-body, #page-header{ clear: both; display: inline-block;}
#page-body:after, #page-header:after{ content: ""; clear: both; display: block;}
#page-body, #page-header{ display: block;}
#page-body-left{ float: left;}
#content-column{ float: right;}

#page-footer{ height: <?php print $footer_height; ?>; position: relative;}

#left-column{ float: left; margin-right: -<?php print $left_column_width; ?>px; width: <?php print $left_column_width; ?>px;}
#right-column{ float: right; margin-left: -<?php print $right_column_width; ?>px; width: <?php print $right_column_width; ?>px;}

body.two-sidebars #page-body-left{ width: <?php print $page_width - $right_column_width - $horizontal_margin; ?>px; margin-right: -<?php print $right_column_width; ?>px;}
body.two-sidebars #content-column{ width: <?php print $page_width - $left_column_width - $right_column_width - $horizontal_margin; ?>px; margin-left: <?php print $left_column_width; ?>px;}

body.sidebar-left #page-body-left{ width: <?php print $page_width - $horizontal_margin; ?>px;}
body.sidebar-left #content-column{ width: <?php print $page_width - $left_column_width - $horizontal_margin; ?>px; margin-left: <?php print $left_column_width; ?>px;}

body.sidebar-right #page-body-left{ width: <?php print $page_width - $right_column_width - $horizontal_margin; ?>px; margin-right: -<?php print $right_column_width; ?>px;}
body.sidebar-right #content-column{ width: <?php print $page_width - $right_column_width - $horizontal_margin; ?>px;}